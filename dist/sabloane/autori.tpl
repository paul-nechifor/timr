{include file="antet.tpl" titlu="Autori"}
<h3>Autori</h3>
<p>Acest sait a fost creat de către:</p>
<ul>
    <li><em>Paul Răzvan Nechifor</em> - anul 2 - 1B</li>
</ul>
<p>&nbsp;</p>
<div class="c2">
<p>Cum am făcut saitul ăsta:</p>
<ul>
    <li>folosesc calculatorul meu cu Arch Linux pe post de <em>server</em> cu Apache și MySQL;</li>
    <li>ca limbaj de programare pe partea <em>server</em>-ului folosesc PHP pentru pagini și Python pentru generarea bazei de date pentru orar (folosind orarul facultății);</li>
    <li>comunicarea cu baza de date este făcută doar într-o clasa PHP intermediară și se folosește mysqli;</li>
    <li>folosesc șabloane Smarty pentru generearea paginilor și părților de pagină (antetul, subsolul, căsuța de autentificare și tabelul pentru orar);</li>
    <li>ca limbaj de programare pe partea de client folosesc JavaScript cu MooTools pentru afișarea ceasului, verificarea datelor de înregistare și profil și pentru animarea căsuțelor de <em>input</em>;</li>
    <li>pot trimite <em>email</em>-uri cu PHP prin GMail pentru recuperearea parolelor pentru conturi;</li>
    <li>toate paginile sunt XHTML 1.0 Strict și sunt valide;</li>
    <li>saitul este minimal și compact; folosește doar 5 imagini de 12&nbsp;KO în total, restul efectelor sunt realizate cu CSS și MooTools;</li>
    <li>sunt folosite fonturi încărcate cu CSS (Cardo și Josefin Sans Std Light);</li>
    <li>am lăsat spații mari între linii și cuvinte pentru a se citi mai bine.</li>
</ul>
</div>
{include file="subsol.tpl"}
